/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
SemaphoreHandle_t Semaforo1;
SemaphoreHandle_t Semaforo2;

QueueHandle_t Cola_Datos;
QueueHandle_t Salida_Datos;
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void StartDefaultTask(void const * argument);

void Toma_Datos(void const * argument);
void Procesa_datos(void const * argument);
void Salida_datos(void const * argument);

/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  Salida_Datos = xQueueCreate( 1, sizeof(uint32_t) );
  Cola_Datos = xQueueCreate( 1, sizeof(uint32_t) );

  Semaforo1 = xSemaphoreCreateBinary();
  Semaforo2 = xSemaphoreCreateBinary();
  xSemaphoreTake(Semaforo2,0);
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* definition and creation of defaultTask */


  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  xTaskCreate((void *)Toma_Datos, "tomaDato", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3, NULL);
  xTaskCreate((void *)Procesa_datos, "procesaDato", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 2, NULL);
  xTaskCreate((void *)Salida_datos, "salidaDato", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 1, NULL);
  /* USER CODE END RTOS_THREADS */

  /* Start scheduler */
  vTaskStartScheduler();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void Toma_Datos(void const * argument)
{
  /* USER CODE BEGIN 5 */
	uint32_t lectura;
	static int cantidad=0;;


  /* Infinite loop */
  for(;;)
  {

	  xSemaphoreTake(Semaforo1, portMAX_DELAY);

	  //lectura del dato del ADC
	  lecura=UNSAM_ADC();
	  xQueueSend(Cola_Datos,&lectura,portMAX_DELAY);
	  cantidad++;

	  xSemaphoreGive(Semaforo1);

	  if (cantidad==4){
		  xSemaphoreTake(Semaforo1, portMAX_DELAY);
		  xSemaphoreGive(Semaforo2);
	      cantidad=0;
	     }
	  vTaskDelay (10 / portTICK_RATE_MS);
  }
  /* USER CODE END 5 */
}

void Procesa_datos(void const * argument)
{
  /* USER CODE BEGIN 5 */
	uint32_t datos[4]={0};
	static float y[4]={0,0,0,0};
	float b1[4]={0.25,0.3,0.35,0.4};
	float b2[4]={1/4,1/4,1/4,1/4};
	int i,j,k,l,m;
	float salida=0,aux=0;
	static int pointer=0;

  /* Infinite loop */
  for(;;)
  {
    	xSemaphoreTake(Semaforo2,portMAX_DELAY);

    	//guardo valores de la cola Cola_Datos en datos[4]
    	for(i=0;i<4;i++){

    		xQueueReceive(Cola_Datos, &datos[i], portMAX_DELAY);
    	}
    	//la salida del primer filtro se calcula con un buffer circular,como ya tenemos los 4 datos, se usa un for para
    	//cambiar el valor de puntero (podría calcularse aux directamente, pero se busca cumplir con el enunciado)

    	for (l=0;l<4;l++){

    		for (m = pointer; m < (pointer + 4) ; m ++ )
    			{
    				aux += datos[m % 4] * b1[m-pointer];
    			}
    		pointer=abs((pointer-1) % 4);
    	}

    	//en el 2do filtro se desplazan los valores un lugar hacia atras, luego aux entra en el ultimo lugar
    	for (j=0;j<3;j++){
    			  y[j]=y[j+1];
    		  }
    	y[3]=aux;

    	//se calcula la salida del segundo filtro
    	for (k=0;k<4;k++){
    			  salida += y[i] * b2[i];
    	}
    	xQueueSend(Salida_Datos,&salida,portMAX_DELAY);
    	xSemaphoreGive(Semaforo1);

  }
  /* USER CODE END 5 */
}

void Salida_datos(void const * argument)
{
  /* USER CODE BEGIN 5 */
	uint32_t salida_final;
  /* Infinite loop */
  for(;;)
  {
	  //si la cola Salida_Datos está vacía, se bloquea
	  xQueueReceive(Salida_Datos, &salida_final, portMAX_DELAY);
	  UNSAM_DAC(salida_final);
  }
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
